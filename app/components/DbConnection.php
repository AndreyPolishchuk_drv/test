<?php
namespace app\components;

use PDO;

/**
 * Class DBConnection
 */
class DBConnection
{
    const CONFIG_PATH = './app/config/db.php';

    /** @var PDO */
    protected static $connection;
    protected static $instance;

    /**
     * DBConnection constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return DBConnection
     */
    public static function getInstance(): DBConnection
    {
        if (!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * @return PDO
     */
    public static function getConnection(): PDO
    {
        if (self::$connection) {
            return self::$connection;
        }

        $dbConfig = self::getDBConfig();

        $dbName = $dbConfig['dbname'];
        $username = $dbConfig['username'];
        $password = $dbConfig['password'];
        $hostname = $dbConfig['hostname'] ?? 'localhost';

        self::$connection = new PDO(
            'mysql:host=' . $hostname . '; dbname=' . $dbName,
            $username, $password
        );

        return self::$connection;
    }

    /**
     * @return array
     */
    protected static function getDBConfig(): array
    {
        return include_once self::CONFIG_PATH;
    }
}