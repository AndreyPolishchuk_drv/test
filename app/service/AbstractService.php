<?php
namespace app\service;

/**
 * Class AbstractService
 */
abstract class AbstractService
{
    public static $services = [];

    /**
     * to support Unit Tests
     * @param string $name
     * @return AbstractService
     */
    public static function getService(string $name): AbstractService
    {
        if (array_key_exists($name, self::$services)) {
            return self::$services[$name];
        }

        return self::$services[$name] = new $name();
    }

    /**
     * @param string $name
     * @param AbstractService $value
     */
    public static function setService(string $name, $value)
    {
        self::$services[$name] = $value;
    }

    /**
     * This is model create method
     * @param array $attributes
     */
    public function createModel(array $attributes)
    {
    }

    /**
     * This is model delete method
     * @param int $id
     */
    public function deleteModel(int $id)
    {
    }

    /**
     * This is model update method
     * @param int $id
     * @param array $sets
     */
    public function updateModel(int $id, array $sets)
    {
    }
}