<?php
namespace app\controllers;

class BaseController
{
    protected $layoutName = 'default';

    public function setLayout($value)
    {
        $this->layoutName = $value;
        return $this;
    }

    public function pagesAccess(): bool
    {
        return isset($_SESSION['login']);
    }

    /**
     * @param $actionName
     * @param null $data
     */
    function render($actionName, $data = null)
    {
    }
}