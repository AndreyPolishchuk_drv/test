<?php
namespace app\controllers;

use app\models\Article;
use app\models\User;
use app\src\repository\ArticleRepository;
use app\src\repository\UserRepository;
use app\src\service\ArticleService;

class ArticleController extends BaseController
{
    /** @var Article */
    public $model;

    /**
     * ArticleController constructor.
     */
    function __construct()
    {
        $this->model = new Article();
    }

    /**
     * @param int $id
     * @return string
     */
    function actionListByUserId(int $id): string
    {
        /**
         *@var User $user
         */
        $user = UserRepository::getByid($id);
        $articles = ArticleRepository::getByUser($user);

        $articleContents = [];
        foreach ($articles as $key => $article) {
            /**
             * @var Article $article
             */
            $articleContents[$key] = $article->getText();
        }

        $this->render('list', [
            'articleContents' => $articleContents,
        ]);
    }

    /**
     * @return false|string
     */
    function actionCreateArticle()
    {
        $data = $_POST['data'];
        $articleService = new ArticleService();
        $result = $articleService->createArticleByData($data);
        return json_encode([
            'success' => $result
        ]);
    }

    /**
     * @return false|string
     */
    function actionGetArticleAuthor()
    {
        /**
         *@var Article $article
         */
        $articleId = $_POST['article_id'];
        $article = ArticleRepository::getByid($articleId);
        $author = $article->getAuthor();

        return json_encode([
            'author' => $author->getName()
        ]);
    }

    /**
     * @return false|string
     */
    function actionChangeArticleAuthor()
    {
        /**
         * @var Article $article
         */
        $articleId = $_POST['article_id'];
        $authorId = $_POST['author_id'];
        $article = ArticleRepository::getByid($articleId);
        $result = $article->setAuthorById($authorId);

        return json_encode([
            'success' => $result
        ]);
    }
}