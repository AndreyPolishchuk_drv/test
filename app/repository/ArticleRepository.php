<?php
namespace app\src\repository;

use app\models\User;

/**
 * Class ArticleRepository
 */
class ArticleRepository
{
    /**
     * This is get by user method
     * @param User $user
     * @return array
     */
    public static function getByUser(User $user): array
    {
    }

    /**
     * This is get by id method
     * @param int $id
     * @return array
     */
    public static function getById(int $id): array
    {
    }
}