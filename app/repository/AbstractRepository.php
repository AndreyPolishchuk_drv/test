<?php
namespace app\src\repository;

use app\components\DBConnection;

class AbstractRepository
{
    protected $connection;

    /**
     * AbstractRepository constructor.
     */
    public function __construct()
    {
        $this->connection = DBConnection::getConnection();
    }

    /**
     * This is find by id method that return array of entities
     * @param int $id
     * @return array
     */
    public function findById(int $id): array
    {
    }
}