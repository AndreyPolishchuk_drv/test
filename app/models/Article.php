<?php
namespace app\models;

/**
 * Class Article
 */
class Article extends BaseModel
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
        ];
    }

    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return 'article';
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
    }

    /**
     * @return User
     */
    public function setAuthorById($id): User
    {
    }

    /**
     * @return string
     */
    public function getText(): string
    {
    }
}