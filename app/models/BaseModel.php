<?php
namespace app\models;

use app\components\DBConnection;
use app\components\validators\Validator;
use PDO;

abstract class BaseModel
{

    protected $connection;
    protected $errors = [];

    /**
     * BaseModel constructor.
     */
    public function __construct()
    {
        $this->connection = DBConnection::getConnection();
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * This is getting rules method
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * This is model validate method
     * @param array $attributes
     * @return bool
     */
    public function validate(array $attributes): bool
    {
    }

    /**
     * @return string
     */
    abstract public static function getTableName(): string;

    /**
     * This is select by sql method
     * @param string $sql
     * @return array
     */
    public function select(string $sql): array
    {
    }
}