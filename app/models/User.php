<?php
namespace app\models;

/**
 * Class User
 */
class User extends BaseModel
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
        ];
    }

    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return 'user';
    }

    /**
     * @return array
     */
    public function getArticles(): array
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
    }
}